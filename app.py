from flask import *
from flask_sqlalchemy import SQLAlchemy
from flask_dance.contrib.google import make_google_blueprint, google
from flask_migrate import Migrate
from flask_dance.consumer import oauth_authorized
import base64
import os

os.system('export OAUTHLIB_INSECURE_TRANSPORT=1')
os.system('export OAUTHLIB_RELAX_TOKEN_SCOPE=1')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root:1234@localhost/users'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app,db)

app.secret_key = "supersekrit"
blueprint = make_google_blueprint(
    client_id="311957068284-u65tpetffvf6ei3fobpc25ta33q1og2f.apps.googleusercontent.com",
    client_secret="ZOcLhOrizM-DUMmNkEXDWuw0e",
    scope=["profile", "email"]
)
app.register_blueprint(blueprint, url_prefix="/login")

"""
app.secret_key = os.environ.get("FLASK_SECRET_KEY", "supersekrit")
app.config["GOOGLE_OAUTH_CLIENT_ID"] = os.environ.get("GOOGLE_OAUTH_CLIENT_ID","311957068284-u65tpetffvf6ei3fobpc25ta33q1og2f.apps.googleusercontent.com")
app.config["GOOGLE_OAUTH_CLIENT_SECRET"] = os.environ.get("GOOGLE_OAUTH_CLIENT_SECRET","ZOcLhOrizM-DUMmNkEXDWuw0")
google_bp = make_google_blueprint(scope=["profile", "email"])
app.register_blueprint(google_bp, url_prefix="/login")

"""
class content(db.Model):
    email = db.Column(db.String(20), primary_key = True)
    password = db.Column(db.String(20),nullable = False)

    def __init__(self,username,password):
        self.email = username
        self.password = password

@app.route('/')
def home():
    return render_template("Login.html")

@app.route('/rr')
def rr():
    return render_template("Registration.html")

@app.route('/welcome')
def welcome():
    return "Welcome User- You have logged in successfully"

@app.route('/login',methods=['POST'])
def login():
    try:
        username = request.form['username']
        password = request.form['password']
        
        user = content.query.filter_by(email = username).first()
        pas =  base64.b64decode(user.password)
        if password == pas:
            return redirect(url_for('welcome'))
        else:
            print "Incorrect password"
            return redirect(url_for('failed'))
    except Exception as e:
        print e
        return redirect(url_for('failed'))
@app.route('/register',methods=['POST'])
def register():
    try:
        username = request.form['email']
        password = request.form['psw']
        r_password = request.form['psw-repeat']
        if password == r_password:
            password = base64.b64encode(r_password)
            user = content(username,password)
            db.session.add(user)
            db.session.commit()
            return "You have registered Successfully"
        else:
            print "Password didn't match"
            return redirect(url_for('failed'))
    except Exception as e:
        print e
        return redirect(url_for('failed'))
@app.route('/failed')
def failed():
    return "Your attempt failed / goto:- https://127.0.0.1:5000/"


@app.route('/googlelog')
def index():
    if not google.authorized:
        return redirect(url_for("google.login"))
    resp = google.get("/plus/v1/people/me")
    assert resp.ok, resp.text
    return "You are {email} on Google".format(email=resp.json()["emails"][0]["value"])  
"""
@oauth_authorized.connect_via(blueprint)
def google_log(blueprint, token):
    info = blueprint.session.get("/plus/v1/people/me")
    email = info.json()["emails"][0]['value']
    return redirect(url_for('/welcome')),email 
"""

if __name__ == '__main__':
   db.create_all()
   app.run(debug=True,ssl_context='adhoc')      
